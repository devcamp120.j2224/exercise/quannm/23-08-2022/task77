$(document).ready(function() {
    // Vùng 1: Khai báo biến global
    var gUrl = "http://localhost:8080/employees";
    var gEmployeeId = 0;
    const gID_COL = 0;
    const gFIRST_NAME_COL = 1;
    const gLAST_NAME_COL = 2;
    const gEXTENSION_COL = 3;
    const gEMAIL_COL = 4;
    const gOFFICE_CODE_COL = 5;
    const gREPORT_TO_COL = 6;
    const gJOB_TITLE_COL = 7;
    const gACTION_COL = 8;
    var gArrayEmployee;
    var gNameCol = ["id", "firstName", "lastName", "extension", "email", "officeCode", "reportTo", "jobTitle", "action"];
    var gTableEmployee = $('#employee-table').DataTable({
      columns: [
        { data: gNameCol[gID_COL] },
        { data: gNameCol[gFIRST_NAME_COL] },
        { data: gNameCol[gLAST_NAME_COL] },
        { data: gNameCol[gEXTENSION_COL] },
        { data: gNameCol[gEMAIL_COL] },
        { data: gNameCol[gOFFICE_CODE_COL] },
        { data: gNameCol[gREPORT_TO_COL] },
        { data: gNameCol[gJOB_TITLE_COL] },
        { data: gNameCol[gACTION_COL] }
      ],
      columnDefs: [
        {
          targets: gACTION_COL,
          defaultContent: `<i class="fas fa-edit text-primary" id="btn-update"></i>
          | <i class="fas fa-trash text-danger" id="btn-delete"></i>`
        }
      ]
    });
  
    // Vùng 2: Vùng gán
    onPageLoading();
    $('#btn-create-employee').on('click', onBtnCreateEmployeeClick);
    $('#employee-table').on('click', "#btn-update", function() {
      onBtnUpdateEmployeeClick(this);
    });
    $('#update-employee').on('click', onBtnUpdateModalClick);
    $('#employee-table').on('click', "#btn-delete", function() {
      onBtnDeleteEmployeeClick(this);
    });
    $('#delete-employee').on('click', onBtnDeleteConfirmClick);
  
    // Vùng 3: Khai báo hàm xử lý sự kiện
    // Hàm xử lý khi tải trang
    function onPageLoading() {
      callApiGetAllEmployee();
      loadDataToTable();
    }
    // Hàm xử lý khi click btn create new employee
    function onBtnCreateEmployeeClick() {
      var vEmployeeObj = {
        firstName: "",
        lastName: "",
        extension: "",
        email: "",
        officeCode: 0,
        reportTo: 0,
        jobTitle: ""
      };
      getDataFromInputCreate(vEmployeeObj);
    //   console.log(vEmployeeObj);
      var vIsCheck = validateData(vEmployeeObj);
      if (vIsCheck == true) {
        console.log(vEmployeeObj);
        callApiPostNewEmployee(vEmployeeObj);
      }
    }
    // Hàm xử lý khi click btn update employee
    function onBtnUpdateEmployeeClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTableEmployee.row(vSelectedRow).data();
      console.log(vDataRow);
      gEmployeeId = vDataRow.id;
      $('#modal-update-employee').modal("show");
      loadDataToModalUpdate(vDataRow);
    }
    // Hàm xử lý khi click btn update in modal
    function onBtnUpdateModalClick() {
        var vEmployeeObj = {
            firstName: "",
            lastName: "",
            extension: "",
            email: "",
            officeCode: 0,
            reportTo: 0,
            jobTitle: ""
        };
      getDataFromInputUpdate(vEmployeeObj);
      // console.log(gCustomerId);
      // console.log(vCustomerObj);
      var vIsCheck = validateData(vEmployeeObj);
      if (vIsCheck == true) {
        callApiPutEmployeeById(vEmployeeObj);
        $('#modal-update-employee').modal("hide");
      }
    }
    // Hàm xử lý khi click btn delete employee
    function onBtnDeleteEmployeeClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTableEmployee.row(vSelectedRow).data();
      console.log(vDataRow);
      gEmployeeId = vDataRow.id;
      $('#modal-delete-employee').modal("show");
    }
    // Hàm xử lý khi click confirm delete
    function onBtnDeleteConfirmClick() {
      callApiDeleteEmployee();
      $('#modal-delete-employee').modal("hide");
    }
  
    // Vùng 4: Khai báo hàm dùng chung
    // Hàm call api get all employee
    function callApiGetAllEmployee() {
      $.ajax({
        url: gUrl,
        type: "GET",
        dataType: "json",
        async: false,
        success: function(res) {
          gArrayEmployee = res;
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm load data to table
    function loadDataToTable() {
      gTableEmployee.clear();
      gTableEmployee.rows.add(gArrayEmployee);
      gTableEmployee.draw();
    }
    // Hàm get data from input create
    function getDataFromInputCreate(paramObj) {
        paramObj.firstName = $('#inp-firstName').val();
        paramObj.lastName = $('#inp-lastName').val();
        paramObj.extension = $('#inp-extension').val();
        paramObj.email = $('#inp-email').val();
        paramObj.officeCode = $('#inp-officeCode').val();
        paramObj.reportTo = $('#inp-reportTo').val();
        paramObj.jobTitle = $('#inp-jobTitle').val();
    }
    // Hàm validate dữ liệu
    function validateData(paramObj) {
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
      if (paramObj.firstName == "") {
        alert("Phải nhập firstName!");
        return false; 
      }
      if (paramObj.lastName == "") {
        alert("Phải nhập description!");
        return false; 
      }
      if (paramObj.email == "") {
        alert("Phải nhập email!");
        return false; 
      }
      if(!pattern.test(paramObj.email)) {
        alert('not a valid e-mail address');
      }
      if (paramObj.jobTitle == "") {
        alert("Phải nhập jobTitle!");
        return false; 
      }
      return true;
    }
    // Hàm call api post
    function callApiPostNewEmployee(paramObj) {
      $.ajax({
        url: gUrl,
        data: JSON.stringify(paramObj),
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          callApiGetAllEmployee();
          loadDataToTable();
          clearInpCreate();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm clear inp create
    function clearInpCreate() {
        $('#inp-firstName').val("");
        $('#inp-lastName').val("");
        $('#inp-extension').val("");
        $('#inp-email').val("");
        $('#inp-officeCode').val("");
        $('#inp-reportTo').val("");
        $('#inp-jobTitle').val("");
    }
    // Hàm load data to modal update
    function loadDataToModalUpdate(paramData) {
      $('#inp-modal-productLineId').val(gEmployeeId);
      $('#inp-modal-firstName').val(paramData.firstName);
      $('#inp-modal-lastName').val(paramData.lastName);
      $('#inp-modal-extension').val(paramData.extension);
      $('#inp-modal-email').val(paramData.email);
      $('#inp-modal-officeCode').val(paramData.officeCode);
      $('#inp-modal-reportTo').val(paramData.reportTo);
      $('#inp-modal-jobTitle').val(paramData.jobTitle);
    }
    // Hàm get data from input update
    function getDataFromInputUpdate(paramObj) {
        paramObj.firstName = $('#inp-modal-firstName').val();
        paramObj.lastName = $('#inp-modal-lastName').val();
        paramObj.extension = $('#inp-modal-extension').val();
        paramObj.email = $('#inp-modal-email').val();
        paramObj.officeCode = $('#inp-modal-officeCode').val();
        paramObj.reportTo = $('#inp-modal-reportTo').val();
        paramObj.jobTitle = $('#inp-modal-jobTitle').val();
    }
    // Hàm call api put
    function callApiPutEmployeeById(paramObj) {
      $.ajax({
        url: gUrl + "/" + gEmployeeId,
        data: JSON.stringify(paramObj),
        type: "PUT",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          callApiGetAllEmployee();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm call api delete
    function callApiDeleteEmployee() {
      $.ajax({
        url: gUrl + "/" + gEmployeeId,
        type: "DELETE",
        async: false,
        success: function(res) {
          callApiGetAllEmployee();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
  });