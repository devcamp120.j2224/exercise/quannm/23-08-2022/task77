$(document).ready(function() {
    // Vùng 1: Khai báo biến global
    var gUrl = "http://localhost:8080/productLines";
    var gProductLineId = 0;
    const gID_COL = 0;
    const gPRODUCT_LINE_NAME_COL = 1;
    const gDESCRIPTTION_COL = 2;
    const gACTION_COL = 3;
    var gArrayProductLine;
    var gNameCol = ["id", "productLine", "description", "action"];
    var gTableProductLine = $('#productLine-table').DataTable({
      columns: [
        { data: gNameCol[gID_COL] },
        { data: gNameCol[gPRODUCT_LINE_NAME_COL] },
        { data: gNameCol[gDESCRIPTTION_COL] },
        { data: gNameCol[gACTION_COL] }
      ],
      columnDefs: [
        {
          targets: gACTION_COL,
          defaultContent: `<i class="fas fa-edit text-primary" id="btn-update"></i>
          | <i class="fas fa-trash text-danger" id="btn-delete"></i>`
        }
      ]
    });
  
    // Vùng 2: Vùng gán
    onPageLoading();
    $('#btn-create-productLine').on('click', onBtnCreateProductLineClick);
    $('#productLine-table').on('click', "#btn-update", function() {
      onBtnUpdateProductLineClick(this);
    });
    $('#update-productLine').on('click', onBtnUpdateModalClick);
    $('#productLine-table').on('click', "#btn-delete", function() {
      onBtnDeleteProductLineClick(this);
    });
    $('#delete-productLine').on('click', onBtnDeleteConfirmClick);
  
    // Vùng 3: Khai báo hàm xử lý sự kiện
    // Hàm xử lý khi tải trang
    function onPageLoading() {
      callApiGetAllProductLine();
      loadDataToTable();
    }
    // Hàm xử lý khi click btn create new productLine
    function onBtnCreateProductLineClick() {
      var vProductLineObj = {
        productLine: "",
        description: ""
      };
      getDataFromInputCreate(vProductLineObj);
      // console.log(vOrderObj);
      var vIsCheck = validateData(vProductLineObj);
      if (vIsCheck == true) {
        console.log(vProductLineObj);
        callApiPostNewProductLine(vProductLineObj);
      }
    }
    // Hàm xử lý khi click btn update productLine
    function onBtnUpdateProductLineClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTableProductLine.row(vSelectedRow).data();
      console.log(vDataRow);
      gProductLineId = vDataRow.id;
      $('#modal-update-productLine').modal("show");
      loadDataToModalUpdate(vDataRow);
    }
    // Hàm xử lý khi click btn update in modal
    function onBtnUpdateModalClick() {
        var vProductLineObj = {
            productLine: "",
            description: ""
          };
      getDataFromInputUpdate(vProductLineObj);
      // console.log(gCustomerId);
      // console.log(vCustomerObj);
      var vIsCheck = validateData(vProductLineObj);
      if (vIsCheck == true) {
        callApiPutProductLineById(vProductLineObj);
        $('#modal-update-productLine').modal("hide");
      }
    }
    // Hàm xử lý khi click btn delete productLine
    function onBtnDeleteProductLineClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTableProductLine.row(vSelectedRow).data();
      console.log(vDataRow);
      gProductLineId = vDataRow.id;
      $('#modal-delete-productLine').modal("show");
    }
    // Hàm xử lý khi click confirm delete
    function onBtnDeleteConfirmClick() {
      callApiDeleteProductLine();
      $('#modal-delete-productLine').modal("hide");
    }
  
    // Vùng 4: Khai báo hàm dùng chung
    // Hàm call api get all productLine
    function callApiGetAllProductLine() {
      $.ajax({
        url: gUrl,
        type: "GET",
        dataType: "json",
        async: false,
        success: function(res) {
          gArrayProductLine = res;
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm load data to table
    function loadDataToTable() {
      gTableProductLine.clear();
      gTableProductLine.rows.add(gArrayProductLine);
      gTableProductLine.draw();
    }
    // Hàm get data from input create
    function getDataFromInputCreate(paramObj) {
      paramObj.productLine = $('#inp-productLine').val();
      paramObj.description = $('#inp-description').val();
    }
    // Hàm validate dữ liệu
    function validateData(paramObj) {
      if (paramObj.productLine == "") {
        alert("Phải nhập productLine!");
        return false; 
      }
      if (paramObj.description == "") {
        alert("Phải nhập description!");
        return false; 
      }
      return true;
    }
    // Hàm call api post
    function callApiPostNewProductLine(paramObj) {
      $.ajax({
        url: gUrl,
        data: JSON.stringify(paramObj),
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          callApiGetAllProductLine();
          loadDataToTable();
          clearInpCreate();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm clear inp create
    function clearInpCreate() {
      $('#inp-productLine').val("");
      $('#inp-description').val("");
    }
    // Hàm load data to modal update
    function loadDataToModalUpdate(paramData) {
      $('#inp-modal-productLineId').val(gProductLineId)
      $('#inp-modal-productLine').val(paramData.productLine);
      $('#inp-modal-description').val(paramData.description);
    }
    // Hàm get data from input update
    function getDataFromInputUpdate(paramObj) {
        paramObj.productLine = $('#inp-modal-productLine').val();
        paramObj.description = $('#inp-modal-description').val();
    }
    // Hàm call api put
    function callApiPutProductLineById(paramObj) {
      $.ajax({
        url: gUrl + "/" + gProductLineId,
        data: JSON.stringify(paramObj),
        type: "PUT",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          callApiGetAllProductLine();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm call api delete
    function callApiDeleteProductLine() {
      $.ajax({
        url: gUrl + "/" + gProductLineId,
        type: "DELETE",
        async: false,
        success: function(res) {
          callApiGetAllProductLine();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
  });