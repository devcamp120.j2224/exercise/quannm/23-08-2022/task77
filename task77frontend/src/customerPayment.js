$(document).ready(function() {
    // Vùng 1: Khai báo biến global
    var gUrl = "http://localhost:8080/payments";
    var gPaymentId = 0;
    var gCustomerName = "";
    const gID_COL = 0;
    const gCHECK_NUM_COL = 1;
    const gPAYMENT_DATE_COL = 2;
    const gAMMOUNT_COL = 3;
    const gCUSTOMER_NAME_COL = 4;
    const gACTION_COL = 5;
    var gArrayPayment;
    var gArrayCustomer;
    var gNameCol = ["id", "checkNumber", "paymentDate", "ammount", "customerName", "action"];
    var gTablePayment = $('#payments-table').DataTable({
      columns: [
        { data: gNameCol[gID_COL] },
        { data: gNameCol[gCHECK_NUM_COL] },
        { data: gNameCol[gPAYMENT_DATE_COL] },
        { data: gNameCol[gAMMOUNT_COL] },
        { data: gNameCol[gCUSTOMER_NAME_COL] },
        { data: gNameCol[gACTION_COL] }
      ],
      columnDefs: [
        {
          targets: gCUSTOMER_NAME_COL,
          render: function() {
            return gCustomerName;
          }
        },
        {
          targets: gACTION_COL,
          defaultContent: `<i class="fas fa-edit text-primary" id="btn-update"></i>
          | <i class="fas fa-trash text-danger" id="btn-delete"></i>`
        }
      ]
    });
    var gHref = new URL(window.location);
    var gCustomerId = gHref.searchParams.get("id");
  
    // Vùng 2: Vùng gán
    onPageLoading();
    $('#create-payment').on('click', onBtnCreatePaymentClick);
    $('#payments-table').on('click', "#btn-update", function() {
      onBtnUpdatePaymentClick(this);
    });
    $('#update-payment').on('click', onBtnUpdateModalClick);
    $('#payments-table').on('click', "#btn-delete", function() {
      onBtnDeletePaymentClick(this);
    });
    $('#delete-payment').on('click', onBtnDeleteConfirmClick);
  
    // Vùng 3: Khai báo hàm xử lý sự kiện
    // Hàm xử lý khi tải trang
    function onPageLoading() {
      callApiGetAllPayment();
      callApiGetCustomerById();
      loadDataToTable();
      callApiGetAllCustomer();
      loadDataToSelectCustomer();
    }
    // Hàm xử lý khi click btn create new payment
    function onBtnCreatePaymentClick() {
      var vPaymentObj = {
        customer: {
          id: 0
        },
        checkNumber: "",
        paymentDate: "",
        ammount: 0,
      };
      getDataFromInputCreate(vPaymentObj);
      // console.log(vPaymentObj);
      var vIsCheck = validateData(vPaymentObj);
      if (vIsCheck == true) {
        console.log(vPaymentObj);
        callApiPostNewPayment(vPaymentObj);
      }
    }
    // Hàm xử lý khi click btn update payment
    function onBtnUpdatePaymentClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTablePayment.row(vSelectedRow).data();
      console.log(vDataRow);
      gPaymentId = vDataRow.id;
      $('#modal-update-payment').modal("show");
      loadDataToModalUpdate(vDataRow);
    }
    // Hàm xử lý khi click btn update in modal
    function onBtnUpdateModalClick() {
      var vPaymentObj = {
        customer: {
          id: 0
        },
        checkNumber: "",
        paymentDate: "",
        ammount: 0,
      };;
      getDataFromInputUpdate(vPaymentObj);
      // console.log(gCustomerId);
      // console.log(vCustomerObj);
      var vIsCheck = validateData(vPaymentObj);
      if (vIsCheck == true) {
        callApiPutPaymentById(vPaymentObj);
        $('#modal-update-payment').modal("hide");
      }
    }
    // Hàm xử lý khi click btn delete payment
    function onBtnDeletePaymentClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTablePayment.row(vSelectedRow).data();
      console.log(vDataRow);
      gPaymentId = vDataRow.id;
      $('#modal-delete-payment').modal("show");
    }
    // Hàm xử lý khi click confirm delete
    function onBtnDeleteConfirmClick() {
      callApiDeletePayment();
      $('#modal-delete-payment').modal("hide");
    }
  
    // Vùng 4: Khai báo hàm dùng chung
    // Hàm call api get all
    function callApiGetAllPayment() {
      $.ajax({
        url: "http://localhost:8080/payments/customerId/" + gCustomerId,
        type: "GET",
        dataType: "json",
        async: false,
        success: function(res) {
          gArrayPayment = res;
          // console.log(gArrayCustomer);
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm load data to table
    function loadDataToTable() {
      gTablePayment.clear();
      gTablePayment.rows.add(gArrayPayment);
      gTablePayment.draw();
    }
    // Hàm get data from input create
    function getDataFromInputCreate(paramObj) {
      var today = new Date().toISOString();
      paramObj.checkNumber = $('#inp-checkNumber').val();
      paramObj.paymentDate = today;
      $('#inp-paymentDate').val(today);
      paramObj.ammount = $('#inp-ammount').val();
      paramObj.customer.id = $('#select-customer').find(":selected").val()
    }
    // Hàm validate dữ liệu
    function validateData(paramObj) {
      var vResult = true;
      if (paramObj.ammount == 0) {
        alert("Phải nhập Ammount!");
        vResult = false;  
      }
      if (paramObj.checkNumber == "") {
        alert("Phải nhập CheckNumber!");
        vResult = false;  
      }
      if (paramObj.customer.id == 0) {
        alert("Phải chọn Customer!");
        vResult = false; 
      }
      return vResult;
    }
    // Hàm call api post
    function callApiPostNewPayment(paramObj) {
      $.ajax({
        url: gUrl,
        data: JSON.stringify(paramObj),
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          callApiGetAllPayment();
          loadDataToTable();
          clearInpCreate();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm clear inp create
    function clearInpCreate() {
      $('#inp-checkNumber').val("");
      $('#inp-paymentDate').val("");
      $('#inp-ammount').val("");
      $('#select-customer').val("0").prop('selected', true);
    }
    // Hàm load data to modal update
    function loadDataToModalUpdate(paramData) {
      $('#inp-modal-paymentId').val(gPaymentId);
      $('#inp-modal-checkNumber').val(paramData.checkNumber);
      $('#inp-modal-paymentDate').val(paramData.paymentDate);
      $('#inp-modal-ammount').val(paramData.ammount);
      $('#select-modal-customer').val(paramData.customer.id).prop('selected', true);
    }
    // Hàm get data from input update
    function getDataFromInputUpdate(paramObj) {
      paramObj.checkNumber = $('#inp-modal-checkNumber').val();
      paramObj.paymentDate = $('#inp-modal-paymentDate').val();
      paramObj.ammount = $('#inp-modal-ammount').val();
      paramObj.customer.id = $('#select-modal-customer').find(":selected").val()
    }
    // Hàm call api put
    function callApiPutPaymentById(paramObj) {
      $.ajax({
        url: gUrl + "/" + gPaymentId,
        data: JSON.stringify(paramObj),
        type: "PUT",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          callApiGetAllPayment();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm call api delete
    function callApiDeletePayment() {
      $.ajax({
        url: gUrl + "/" + gPaymentId,
        type: "DELETE",
        async: false,
        success: function(res) {
          callApiGetAllPayment();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm call api get all
    function callApiGetAllCustomer() {
      $.ajax({
        url: "http://localhost:8080/customers",
        type: "GET",
        dataType: "json",
        async: false,
        success: function(res) {
          gArrayCustomer = res;
          // console.log(gArrayCustomer);
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm load select customer
    function loadDataToSelectCustomer() {
      for (var i = 0; i < gArrayCustomer.length; i++) {
        $('#select-customer').append('<option value="' + gArrayCustomer[i].id + '">' + gArrayCustomer[i].firstName + " " + gArrayCustomer[i].lastName + '</option>');
        $('#select-modal-customer').append('<option value="' + gArrayCustomer[i].id + '">' + gArrayCustomer[i].firstName + " " + gArrayCustomer[i].lastName + '</option>');
      }
    }
    // Hàm get api lấy tt customer
    function callApiGetCustomerById() {
        $.ajax({
            url: "http://localhost:8080/customers/" + gCustomerId,
            type: "GET",
            dataType: "json",
            async: false,
            success: function(res) {
              gCustomerName = res.firstName + " " + res.lastName;
              console.log(gCustomerName);
            },
            error: function(error) {
              console.log(error.responseText);
            }
          });
    }
  });