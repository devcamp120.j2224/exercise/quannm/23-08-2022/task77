$(document).ready(function() {
  // Vùng 1: Khai báo biến global
  var gUrl = "http://localhost:8080/products";
  var gProductId = 0;
  const gID_COL = 0;
  const gPRODUCT_CODE_COL = 1;
  const gPRODUCT_NAME_COL = 2;
  const gPRODUCT_DESCRIPTTION_COL = 3;
  const gPRODUCT_LINE_COL = 4;
  const gPRODUCT_SCALE_COL = 5;
  const gPRODUCT_VENDER_COL = 6;
  const gQUANTITY_IN_STOCK_COL = 7;
  const gBUY_PRICE_COL = 8;
  const gACTION_COL = 9;
  var gArrayProductLine;
  var gArrayProduct;
  var gNameCol = ["id", "productCode", "productName", "productDescripttion", "productLine.productLine", "productScale", "productVendor", "quantityInStock", "buyPrice", "action"];
  var gTableProduct = $('#product-table').DataTable({
    columns: [
      { data: gNameCol[gID_COL] },
      { data: gNameCol[gPRODUCT_CODE_COL] },
      { data: gNameCol[gPRODUCT_NAME_COL] },
      { data: gNameCol[gPRODUCT_DESCRIPTTION_COL] },
      { data: gNameCol[gPRODUCT_LINE_COL] },
      { data: gNameCol[gPRODUCT_SCALE_COL] },
      { data: gNameCol[gPRODUCT_VENDER_COL] },
      { data: gNameCol[gQUANTITY_IN_STOCK_COL] },
      { data: gNameCol[gBUY_PRICE_COL] },
      { data: gNameCol[gACTION_COL] }
    ],
    columnDefs: [
      {
        targets: gACTION_COL,
        defaultContent: `<i class="fas fa-edit text-primary" id="btn-update"></i>
        | <i class="fas fa-trash text-danger" id="btn-delete"></i>`
      }
    ]
  });

  // Vùng 2: Vùng gán
  onPageLoading();
  $('#btn-create-product').on('click', onBtnCreateProductClick);
  $('#select-productLine').on('change', onChangeSelectProductLine);
  $('#select-modal-productLine').on('change', onChangeSelectProductLineInModal);
  $('#product-table').on('click', "#btn-update", function() {
    onBtnUpdateProductClick(this);
  });
  $('#update-product').on('click', onBtnUpdateModalClick);
  $('#product-table').on('click', "#btn-delete", function() {
    onBtnDeleteProductClick(this);
  });
  $('#delete-product').on('click', onBtnDeleteConfirmClick);

  // Vùng 3: Khai báo hàm xử lý sự kiện
  // Hàm xử lý khi tải trang
  function onPageLoading() {
    callApiGetAllProduct();
    loadDataToTable();
    callApiGetAllProductLine();
    loadDataToSelectProductLine();
  }
  // Hàm xử lý khi click btn create new product
  function onBtnCreateProductClick() {
    var vProductObj = {
      productCode: "",
      productName: "",
      productDescripttion: "",
      productLine: {
        id: 0
      },
      idProductLine: 0,
      productScale: "",
      productVendor: "",
      quantityInStock: 0,
      buyPrice: 0
    };
    getDataFromInputCreate(vProductObj);
    // console.log(vOrderObj);
    var vIsCheck = validateData(vProductObj);
    if (vIsCheck == true) {
      console.log(vProductObj);
      callApiPostNewProduct(vProductObj);
    }
  }
  // Hàm xử lý khi thay đổi lựa chọn productLine
  function onChangeSelectProductLine() {
    var vSelectedProductLine = $('#select-productLine').find(":selected").val();
    $('#inp-idProductLine').val(vSelectedProductLine);
  }
  // Hàm xử lý khi thay đổi lựa chọn productLine in modal
  function onChangeSelectProductLineInModal() {
    var vSelectedProductLine = $('#select-modal-productLine').find(":selected").val();
    $('#inp-modal-idProductLine').val(vSelectedProductLine);
  }
  // Hàm xử lý khi click btn update product
  function onBtnUpdateProductClick(paramEle) {
    var vSelectedRow = $(paramEle).parents("tr");
    var vDataRow = gTableProduct.row(vSelectedRow).data();
    console.log(vDataRow);
    gProductId = vDataRow.id;
    $('#modal-update-product').modal("show");
    loadDataToModalUpdate(vDataRow);
  }
  // Hàm xử lý khi click btn update in modal
  function onBtnUpdateModalClick() {
    var vProductObj = {
      productCode: "",
      productName: "",
      productDescripttion: "",
      productLine: {
        id: 0
      },
      idProductLine: 0,
      productScale: "",
      productVendor: "",
      quantityInStock: 0,
      buyPrice: 0
    };
    getDataFromInputUpdate(vProductObj);
    // console.log(gCustomerId);
    // console.log(vCustomerObj);
    var vIsCheck = validateData(vProductObj);
    if (vIsCheck == true) {
      callApiPutProductById(vProductObj);
      $('#modal-update-product').modal("hide");
    }
  }
  // Hàm xử lý khi click btn delete product
  function onBtnDeleteProductClick(paramEle) {
    var vSelectedRow = $(paramEle).parents("tr");
    var vDataRow = gTableProduct.row(vSelectedRow).data();
    console.log(vDataRow);
    gProductId = vDataRow.id;
    $('#modal-delete-product').modal("show");
  }
  // Hàm xử lý khi click confirm delete
  function onBtnDeleteConfirmClick() {
    callApiDeleteProduct();
    $('#modal-delete-product').modal("hide");
  }

  // Vùng 4: Khai báo hàm dùng chung
  // Hàm call api get all product
  function callApiGetAllProduct() {
    $.ajax({
      url: gUrl,
      type: "GET",
      dataType: "json",
      async: false,
      success: function(res) {
        gArrayProduct = res;
        // console.log(gArrayCustomer);
      },
      error: function(error) {
        console.log(error.responseText);
      }
    });
  }
  // Hàm load data to table
  function loadDataToTable() {
    gTableProduct.clear();
    gTableProduct.rows.add(gArrayProduct);
    gTableProduct.draw();
  }
  // Hàm get data from input create
  function getDataFromInputCreate(paramObj) {
    paramObj.productCode = $('#inp-productCode').val();
    paramObj.productName = $('#inp-productName').val();
    paramObj.productDescripttion = $('#inp-productDescripttion').val();
    paramObj.productLine.id = $('#select-productLine').find(":selected").val();
    paramObj.idProductLine = $('#inp-idProductLine').val();
    paramObj.productScale = $('#inp-productScale').val();
    paramObj.productVendor = $('#inp-productVendor').val();
    paramObj.quantityInStock = $('#inp-quantityInStock').val();
    paramObj.buyPrice = $('#inp-buyPrice').val();
  }
  // Hàm validate dữ liệu
  function validateData(paramObj) {
    if (paramObj.productCode == "") {
      alert("Phải nhập productCode!");
      return false; 
    }
    if (paramObj.productName == "") {
      alert("Phải nhập productName!");
      return false; 
    }
    if (paramObj.productLine.id == 0) {
      alert("Phải chọn productLine!");
      return false; 
    }
    if (paramObj.quantityInStock == 0) {
      alert("Phải nhập quantityInStock!");
      return false; 
    }
    if (paramObj.buyPrice == 0) {
      alert("Phải nhập buyPrice!");
      return false; 
    }
    return true;
  }
  // Hàm call api post
  function callApiPostNewProduct(paramObj) {
    $.ajax({
      url: gUrl,
      data: JSON.stringify(paramObj),
      type: "POST",
      dataType: "json",
      contentType: 'application/json',
      async: false,
      success: function(res) {
        callApiGetAllProduct();
        loadDataToTable();
        clearInpCreate();
      },
      error: function(error) {
        console.log(error.responseText);
      }
    });
  }
  // Hàm clear inp create
  function clearInpCreate() {
    $('#inp-productCode').val("");
    $('#inp-productName').val("");
    $('#inp-productDescripttion').val("");
    $('#select-productLine').val("0").prop('selected', true);
    $('#inp-idProductLine').val("");
    $('#inp-productScale').val("");
    $('#inp-productVendor').val("");
    $('#inp-quantityInStock').val("");
    $('#inp-buyPrice').val("");
  }
  // Hàm load data to modal update
  function loadDataToModalUpdate(paramData) {
    $('#inp-modal-productId').val(gProductId)
    $('#inp-modal-productCode').val(paramData.productCode);
    $('#inp-modal-productName').val(paramData.productName);
    $('#inp-modal-productDescripttion').val(paramData.productDescripttion);
    $('#select-modal-productLine').val(paramData.productLine.id).prop('selected', true);
    $('#inp-modal-idProductLine').val(paramData.idProductLine);
    $('#inp-modal-productScale').val(paramData.productScale);
    $('#inp-modal-productVendor').val(paramData.productVendor);
    $('#inp-modal-quantityInStock').val(paramData.quantityInStock);
    $('#inp-modal-buyPrice').val(paramData.buyPrice);
  }
  // Hàm get data from input update
  function getDataFromInputUpdate(paramObj) {
    paramObj.productCode = $('#inp-modal-productCode').val();
    paramObj.productName = $('#inp-modal-productName').val();
    paramObj.productDescripttion = $('#inp-modal-productDescripttion').val();
    paramObj.productLine.id = $('#select-modal-productLine').find(":selected").val();
    paramObj.idProductLine = $('#inp-modal-idProductLine').val();
    paramObj.productScale = $('#inp-modal-productScale').val();
    paramObj.productVendor = $('#inp-modal-productVendor').val();
    paramObj.quantityInStock = $('#inp-modal-quantityInStock').val();
    paramObj.buyPrice = $('#inp-modal-buyPrice').val();
  }
  // Hàm call api put
  function callApiPutProductById(paramObj) {
    $.ajax({
      url: gUrl + "/" + gProductId,
      data: JSON.stringify(paramObj),
      type: "PUT",
      contentType: 'application/json',
      async: false,
      success: function(res) {
        callApiGetAllProduct();
        loadDataToTable();
      },
      error: function(error) {
        console.log(error.responseText);
      }
    });
  }
  // Hàm call api delete
  function callApiDeleteProduct() {
    $.ajax({
      url: gUrl + "/" + gProductId,
      type: "DELETE",
      async: false,
      success: function(res) {
        callApiGetAllProduct();
        loadDataToTable();
      },
      error: function(error) {
        console.log(error.responseText);
      }
    });
  }
  // Hàm call api get all productLine
  function callApiGetAllProductLine() {
    $.ajax({
      url: "http://localhost:8080/productLines",
      type: "GET",
      dataType: "json",
      async: false,
      success: function(res) {
        gArrayProductLine = res;
        // console.log(gArrayCustomer);
      },
      error: function(error) {
        console.log(error.responseText);
      }
    });
  }
  // Hàm load select product
  function loadDataToSelectProductLine() {
    for (var i = 0; i < gArrayProductLine.length; i++) {
      $('#select-productLine').append('<option value="' + gArrayProductLine[i].id + '">' + gArrayProductLine[i].productLine + '</option>');
      $('#select-modal-productLine').append('<option value="' + gArrayProductLine[i].id + '">' + gArrayProductLine[i].productLine + '</option>');
    }
  }
});