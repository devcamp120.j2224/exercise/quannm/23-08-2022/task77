$(document).ready(function() {
    // Vùng 1: Khai báo biến global
    var gUrl = "http://localhost:8080/orderDetails";
    var gOrderDetailId = 0;
    const gID_COL = 0;
    const gORDER_ID_COL = 1;
    const gCUSTOMER_NAME_COL = 2;
    const gPRODUCT_NAME_COL = 3;
    const gQUANTITY_ORDER_COL = 4;
    const gPRICE_EACH_COL = 5;
    const gACTION_COL = 6
    var gArrayOrderDetail;
    var gArrayOrder;
    var gArrayProduct;
    var gNameCol = ["id", "order.id", "customerName", "product.productName", "quantityOrder", "priceEach", "action"];
    var gTableOrderDetail = $('#orderDetail-table').DataTable({
      columns: [
        { data: gNameCol[gID_COL] },
        { data: gNameCol[gORDER_ID_COL] },
        { data: gNameCol[gCUSTOMER_NAME_COL] },
        { data: gNameCol[gPRODUCT_NAME_COL] },
        { data: gNameCol[gQUANTITY_ORDER_COL] },
        { data: gNameCol[gPRICE_EACH_COL] },
        { data: gNameCol[gACTION_COL] }
      ],
      columnDefs: [
        {
            targets: gCUSTOMER_NAME_COL,
            render: function() {
                return gCustomeName;
            }
        },
        {
          targets: gACTION_COL,
          defaultContent: `<i class="fas fa-edit text-primary" id="btn-update"></i>
          | <i class="fas fa-trash text-danger" id="btn-delete"></i>`
        }
      ]
    });
    var gHref = new URL(window.location);
    var gOrderId = gHref.searchParams.get("id");
    var gCustomeName = "";
  
    // Vùng 2: Vùng gán
    onPageLoading();
    $('#create-orderDetail').on('click', onBtnCreateOrderDetailClick);
    $('#orderDetail-table').on('click', "#btn-update", function() {
      onBtnUpdateOrderDetailClick(this);
    });
    $('#update-orderDetail').on('click', onBtnUpdateModalClick);
    $('#orderDetail-table').on('click', "#btn-delete", function() {
      onBtnDeleteOrderDetailClick(this);
    });
    $('#delete-orderDetail').on('click', onBtnDeleteConfirmClick);
  
    // Vùng 3: Khai báo hàm xử lý sự kiện
    // Hàm xử lý khi tải trang
    function onPageLoading() {
      callApiGetAllOrderDetail();
      loadDataToTable();
      callApiGetAllOrder();
      callApiGetAllProduct();
      loadDataToSelectOrder();
      loadDataToSelectProduct();
    }
    // Hàm xử lý khi click btn create new orderDetail
    function onBtnCreateOrderDetailClick() {
      var vOrderDetailObj = {
        order: {
          id: 0
        },
        product: {
          id: 0
        },
        quantityOrder: 0,
        priceEach: 0
      };
      getDataFromInputCreate(vOrderDetailObj);
      // console.log(vOrderObj);
      var vIsCheck = validateData(vOrderDetailObj);
      if (vIsCheck == true) {
        console.log(vOrderDetailObj);
        callApiPostNewOrderDetail(vOrderDetailObj);
      }
    }
    // Hàm xử lý khi click btn update orderDetail
    function onBtnUpdateOrderDetailClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTableOrderDetail.row(vSelectedRow).data();
      console.log(vDataRow);
      gOrderDetailId = vDataRow.id;
      $('#modal-update-orderDetail').modal("show");
      loadDataToModalUpdate(vDataRow);
    }
    // Hàm xử lý khi click btn update in modal
    function onBtnUpdateModalClick() {
      var vOrderDetailObj = {
        order: {
          id: 0
        },
        product: {
          id: 0
        },
        quantityOrder: 0,
        priceEach: 0
      };
      getDataFromInputUpdate(vOrderDetailObj);
      // console.log(gCustomerId);
      // console.log(vCustomerObj);
      var vIsCheck = validateData(vOrderDetailObj);
      if (vIsCheck == true) {
        callApiPutOrderDetailById(vOrderDetailObj);
        $('#modal-update-orderDetail').modal("hide");
      }
    }
    // Hàm xử lý khi click btn delete orderDetail
    function onBtnDeleteOrderDetailClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTableOrderDetail.row(vSelectedRow).data();
      console.log(vDataRow);
      gOrderDetailId = vDataRow.id;
      $('#modal-delete-orderDetail').modal("show");
    }
    // Hàm xử lý khi click confirm delete
    function onBtnDeleteConfirmClick() {
      callApiDeleteOrderDetail();
      $('#modal-delete-orderDetail').modal("hide");
    }
  
    // Vùng 4: Khai báo hàm dùng chung
    // Hàm call api get all
    function callApiGetAllOrderDetail() {
      $.ajax({
        url: "http://localhost:8080/orderDetails/orderId/" + gOrderId,
        type: "GET",
        dataType: "json",
        async: false,
        success: function(res) {
          gArrayOrderDetail = res;
          gCustomeName = gArrayOrderDetail[0].order.customer.firstName + " " + gArrayOrderDetail[0].order.customer.lastName;
          // console.log(gArrayCustomer);
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm load data to table
    function loadDataToTable() {
      gTableOrderDetail.clear();
      gTableOrderDetail.rows.add(gArrayOrderDetail);
      gTableOrderDetail.draw();
    }
    // Hàm get data from input create
    function getDataFromInputCreate(paramObj) {
      paramObj.quantityOrder = $('#inp-quantityOrder').val();
      paramObj.priceEach = $('#inp-priceEach').val();
      paramObj.order.id = $('#select-order').find(":selected").val();
      paramObj.product.id = $('#select-product').find(":selected").val();
    }
    // Hàm validate dữ liệu
    function validateData(paramObj) {
      if (paramObj.order.id == 0) {
        alert("Phải chọn order!");
        return false; 
      }
      if (paramObj.product.id == 0) {
        alert("Phải chọn product!");
        return false; 
      }
      if (paramObj.quantityOrder == 0) {
        alert("Phải nhập quantityOrder!");
        return false; 
      }
      if (paramObj.priceEach == 0) {
        alert("Phải nhập priceEach!");
        return false; 
      }
      return true;
    }
    // Hàm call api post
    function callApiPostNewOrderDetail(paramObj) {
      $.ajax({
        url: gUrl,
        data: JSON.stringify(paramObj),
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          callApiGetAllOrderDetail();
          loadDataToTable();
          clearInpCreate();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm clear inp create
    function clearInpCreate() {
      $('#inp-quantityOrder').val("");
      $('#inp-priceEach').val("");
      $('#select-order').val("0").prop('selected', true);
      $('#select-product').val("0").prop('selected', true);
    }
    // Hàm load data to modal update
    function loadDataToModalUpdate(paramData) {
      $('#inp-modal-orderDetailId').val(gOrderDetailId)
      $('#inp-modal-quantityOrder').val(paramData.quantityOrder);
      $('#inp-modal-priceEach').val(paramData.priceEach);
      $('#select-modal-order').val(paramData.order.id).prop('selected', true);
      $('#select-modal-product').val(paramData.product.id).prop('selected', true);
    }
    // Hàm get data from input update
    function getDataFromInputUpdate(paramObj) {
      paramObj.quantityOrder = $('#inp-modal-quantityOrder').val();
      paramObj.priceEach = $('#inp-modal-priceEach').val();
      paramObj.order.id = $('#select-modal-order').find(":selected").val();
      paramObj.product.id = $('#select-modal-product').find(":selected").val();
    }
    // Hàm call api put
    function callApiPutOrderDetailById(paramObj) {
      $.ajax({
        url: gUrl + "/" + gOrderDetailId,
        data: JSON.stringify(paramObj),
        type: "PUT",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          callApiGetAllOrderDetail();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm call api delete
    function callApiDeleteOrderDetail() {
      $.ajax({
        url: gUrl + "/" + gOrderDetailId,
        type: "DELETE",
        async: false,
        success: function(res) {
          callApiGetAllOrderDetail();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm call api get all order
    function callApiGetAllOrder() {
      $.ajax({
        url: "http://localhost:8080/orders",
        type: "GET",
        dataType: "json",
        async: false,
        success: function(res) {
          gArrayOrder = res;
          // console.log(gArrayCustomer);
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm call api get all product
    function callApiGetAllProduct() {
      $.ajax({
        url: "http://localhost:8080/products",
        type: "GET",
        dataType: "json",
        async: false,
        success: function(res) {
          gArrayProduct = res;
          // console.log(gArrayCustomer);
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm load select order
    function loadDataToSelectOrder() {
      for (var i = 0; i < gArrayOrder.length; i++) {
        $('#select-order').append('<option value="' + gArrayOrder[i].id + '">' + gArrayOrder[i].id + '</option>');
        $('#select-modal-order').append('<option value="' + gArrayOrder[i].id + '">' + gArrayOrder[i].id + '</option>');
      }
    }
    // Hàm load select product
    function loadDataToSelectProduct() {
      for (var i = 0; i < gArrayProduct.length; i++) {
        $('#select-product').append('<option value="' + gArrayProduct[i].id + '">' + gArrayProduct[i].productName + '</option>');
        $('#select-modal-product').append('<option value="' + gArrayProduct[i].id + '">' + gArrayProduct[i].productName + '</option>');
      }
    }
  });