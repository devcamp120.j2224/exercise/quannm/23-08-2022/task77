$(document).ready(function() {
    // Vùng 1: Khai báo biến global
    var gUrl = "http://localhost:8080/offices";
    var gOfficeId = 0;
    const gID_COL = 0;
    const gADDRESS_LINE_COL = 1;
    const gCITY_COL = 2;
    const gSTATE_COL = 3;
    const gCOUNTRY_COL = 4;
    const gTERRITORY_COL = 5;
    const gPHONE_COL = 6;
    const gACTION_COL = 7;
    var gArrayOffice;
    var gNameCol = ["id", "addressLine", "city", "state", "country", "territory", "phone", "action"];
    var gTableOffice = $('#office-table').DataTable({
      columns: [
        { data: gNameCol[gID_COL] },
        { data: gNameCol[gADDRESS_LINE_COL] },
        { data: gNameCol[gCITY_COL] },
        { data: gNameCol[gSTATE_COL] },
        { data: gNameCol[gCOUNTRY_COL] },
        { data: gNameCol[gTERRITORY_COL] },
        { data: gNameCol[gPHONE_COL] },
        { data: gNameCol[gACTION_COL] }
      ],
      columnDefs: [
        {
          targets: gACTION_COL,
          defaultContent: `<i class="fas fa-edit text-primary" id="btn-update"></i>
          | <i class="fas fa-trash text-danger" id="btn-delete"></i>`
        }
      ]
    });
  
    // Vùng 2: Vùng gán
    onPageLoading();
    $('#btn-create-office').on('click', onBtnCreateOfficeClick);
    $('#office-table').on('click', "#btn-update", function() {
      onBtnUpdateOfficeClick(this);
    });
    $('#update-office').on('click', onBtnUpdateModalClick);
    $('#office-table').on('click', "#btn-delete", function() {
      onBtnDeleteOfficeClick(this);
    });
    $('#delete-office').on('click', onBtnDeleteConfirmClick);
  
    // Vùng 3: Khai báo hàm xử lý sự kiện
    // Hàm xử lý khi tải trang
    function onPageLoading() {
      callApiGetAllOffice();
      loadDataToTable();
    }
    // Hàm xử lý khi click btn create new office
    function onBtnCreateOfficeClick() {
      var vOfficeObj = {
        addressLine: "",
        city: "",
        state: "",
        country: "",
        territory: "",
        phone: ""
      };
      getDataFromInputCreate(vOfficeObj);
    //   console.log(vEmployeeObj);
      var vIsCheck = validateData(vOfficeObj);
      if (vIsCheck == true) {
        console.log(vOfficeObj);
        callApiPostNewOffice(vOfficeObj);
      }
    }
    // Hàm xử lý khi click btn update office
    function onBtnUpdateOfficeClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTableOffice.row(vSelectedRow).data();
      console.log(vDataRow);
      gOfficeId = vDataRow.id;
      $('#modal-update-office').modal("show");
      loadDataToModalUpdate(vDataRow);
    }
    // Hàm xử lý khi click btn update in modal
    function onBtnUpdateModalClick() {
        var vOfficeObj = {
            addressLine: "",
            city: "",
            state: "",
            country: "",
            territory: "",
            phone: ""
          };
      getDataFromInputUpdate(vOfficeObj);
      // console.log(gCustomerId);
      // console.log(vCustomerObj);
      var vIsCheck = validateData(vOfficeObj);
      if (vIsCheck == true) {
        callApiPutOfficeById(vOfficeObj);
        $('#modal-update-office').modal("hide");
      }
    }
    // Hàm xử lý khi click btn delete office
    function onBtnDeleteOfficeClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTableOffice.row(vSelectedRow).data();
      console.log(vDataRow);
      gOfficeId = vDataRow.id;
      $('#modal-delete-office').modal("show");
    }
    // Hàm xử lý khi click confirm delete
    function onBtnDeleteConfirmClick() {
      callApiDeleteOffice();
      $('#modal-delete-office').modal("hide");
    }
  
    // Vùng 4: Khai báo hàm dùng chung
    // Hàm call api get all office
    function callApiGetAllOffice() {
      $.ajax({
        url: gUrl,
        type: "GET",
        dataType: "json",
        async: false,
        success: function(res) {
          gArrayOffice = res;
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm load data to table
    function loadDataToTable() {
      gTableOffice.clear();
      gTableOffice.rows.add(gArrayOffice);
      gTableOffice.draw();
    }
    // Hàm get data from input create
    function getDataFromInputCreate(paramObj) {
        paramObj.addressLine = $('#inp-addressLine').val();
        paramObj.city = $('#inp-city').val();
        paramObj.state = $('#inp-state').val();
        paramObj.country = $('#inp-country').val();
        paramObj.territory = $('#inp-territory').val();
        paramObj.phone = $('#inp-phone').val();
    }
    // Hàm validate dữ liệu
    function validateData(paramObj) {
      if (paramObj.addressLine == "") {
        alert("Phải nhập addressLine!");
        return false; 
      }
      if (paramObj.city == "") {
        alert("Phải nhập city!");
        return false; 
      }
      if (paramObj.state == "") {
        alert("Phải nhập state!");
        return false; 
      }
      if (paramObj.country == "") {
        alert("Phải nhập country!");
        return false; 
      }
      if (paramObj.territory == "") {
        alert("Phải nhập territory!");
        return false; 
      }
      if (paramObj.phone == "") {
        alert("Phải nhập phone!");
        return false; 
      }
      return true;
    }
    // Hàm call api post
    function callApiPostNewOffice(paramObj) {
      $.ajax({
        url: gUrl,
        data: JSON.stringify(paramObj),
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          callApiGetAllOffice();
          loadDataToTable();
          clearInpCreate();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm clear inp create
    function clearInpCreate() {
        $('#inp-addressLine').val("");
        $('#inp-city').val("");
        $('#inp-state').val("");
        $('#inp-country').val("");
        $('#inp-territory').val("");
        $('#inp-phone').val("");
    }
    // Hàm load data to modal update
    function loadDataToModalUpdate(paramData) {
      $('#inp-modal-officeId').val(gOfficeId);
      $('#inp-modal-addressLine').val(paramData.addressLine);
      $('#inp-modal-city').val(paramData.city);
      $('#inp-modal-state').val(paramData.state);
      $('#inp-modal-country').val(paramData.country);
      $('#inp-modal-territory').val(paramData.territory);
      $('#inp-modal-phone').val(paramData.phone);
    }
    // Hàm get data from input update
    function getDataFromInputUpdate(paramObj) {
        paramObj.addressLine = $('#inp-modal-addressLine').val();
        paramObj.city = $('#inp-modal-city').val();
        paramObj.state = $('#inp-modal-state').val();
        paramObj.country = $('#inp-modal-country').val();
        paramObj.territory = $('#inp-modal-territory').val();
        paramObj.phone = $('#inp-modal-phone').val();
    }
    // Hàm call api put
    function callApiPutOfficeById(paramObj) {
      $.ajax({
        url: gUrl + "/" + gOfficeId,
        data: JSON.stringify(paramObj),
        type: "PUT",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          callApiGetAllOffice();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm call api delete
    function callApiDeleteOffice() {
      $.ajax({
        url: gUrl + "/" + gOfficeId,
        type: "DELETE",
        async: false,
        success: function(res) {
          callApiGetAllOffice();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
  });