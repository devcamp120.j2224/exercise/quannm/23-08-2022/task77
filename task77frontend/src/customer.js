$(document).ready(function() {
  // Vùng 1: Khai báo biến global
  var gUrl = "http://localhost:8080/customers";
  var gCustomerId = 0;
  const gID_COL = 0;
  const gFIRST_NAME_COL = 1;
  const gLAST_NAME_COL = 2;
  const gPHONE_NUM_COL = 3;
  const gADDRESS_COL = 4;
  const gCITY_COL = 5;
  const gSTATE_COL = 6;
  const gCOUNTRY_COL = 7;
  const gCREDIT_LIMIT_COL = 8;
  const gACTION_COL = 9;
  var gArrayCustomer;
  var gNameCol = ["id", "firstName", "lastName", "phoneNumber", "address", "city", "state", "country", "creditLimit", "action"];
  var gTableCustomer = $('#customer-table').DataTable({
    columns: [
      { data: gNameCol[gID_COL] },
      { data: gNameCol[gFIRST_NAME_COL] },
      { data: gNameCol[gLAST_NAME_COL] },
      { data: gNameCol[gPHONE_NUM_COL] },
      { data: gNameCol[gADDRESS_COL] },
      { data: gNameCol[gCITY_COL] },
      { data: gNameCol[gSTATE_COL] },
      { data: gNameCol[gCOUNTRY_COL] },
      { data: gNameCol[gCREDIT_LIMIT_COL] },
      { data: gNameCol[gACTION_COL] }
    ],
    columnDefs: [
      {
        targets: gACTION_COL,
        defaultContent: `<i class="fas fa-edit text-primary" id="btn-update"></i>
        | <i class="fas fa-trash text-danger" id="btn-delete"></i>
        <button class="btn btn-success" id="btn-detail-order">Detail Orders</button>
        <button class="btn btn-info" id="btn-detail-payment">Detail Payments</button>`
      }
    ]
  });

  // Vùng 2: Vùng gán
  onPageLoading();
  $('#create-customer').on('click', onBtnCreateCustomerClick);
  $('#customer-table').on('click', "#btn-update", function() {
    onBtnUpdateCustomerClick(this);
  });
  $('#btn-modal-update-customer').on('click', onBtnUpdateModalClick);
  $('#customer-table').on('click', "#btn-delete", function() {
    onBtnDeleteCustomerClick(this);
  });
  $('#btn-confirm-delete-customer').on('click', onBtnDeleteConfirmClick);
  $('#customer-table').on('click', "#btn-detail-order", function() {
    onBtnDetailOrderClick(this);
  });
  $('#customer-table').on('click', "#btn-detail-payment", function() {
    onBtnDetailPaymentClick(this);
  });

  // Vùng 3: Khai báo hàm xử lý sự kiện
  // Hàm xử lý khi tải trang
  function onPageLoading() {
    callApiGetAllCustomer();
    loadDataToTable();
  }
  // Hàm xử lý khi click btn create new customer
  function onBtnCreateCustomerClick() {
    var vCustomerObj = {
      lastName: "",
      firstName: "",
      phoneNumber: "",
      address: "",
      city: "",
      state: "",
      postalCode: "",
      country: "",
      salesRepEmployeeNumber: 0,
      creditLimit: 0
    };
    getDataFromInputCreate(vCustomerObj);
    // console.log(vCustomerObj);
    var vIsCheck = validateData(vCustomerObj);
    if (vIsCheck == true) {
      callApiPostNewCustomer(vCustomerObj);
    }
  }
  // Hàm xử lý khi click btn update customer
  function onBtnUpdateCustomerClick(paramEle) {
    var vSelectedRow = $(paramEle).parents("tr");
    var vDataRow = gTableCustomer.row(vSelectedRow).data();
    console.log(vDataRow);
    gCustomerId = vDataRow.id;
    $('#modal-update-customer').modal("show");
    loadDataToModalUpdate(vDataRow);
  }
  // Hàm xử lý khi click btn update in modal
  function onBtnUpdateModalClick() {
    var vCustomerObj = {
      lastName: "",
      firstName: "",
      phoneNumber: "",
      address: "",
      city: "",
      state: "",
      postalCode: "",
      country: "",
      salesRepEmployeeNumber: 0,
      creditLimit: 0
    };
    getDataFromInputUpdate(vCustomerObj);
    // console.log(gCustomerId);
    // console.log(vCustomerObj);
    var vIsCheck = validateData(vCustomerObj);
    if (vIsCheck == true) {
      callApiPutCustomerById(vCustomerObj);
      $('#modal-update-customer').modal("hide");
    }
  }
  // Hàm xử lý khi click btn delete customer
  function onBtnDeleteCustomerClick(paramEle) {
    var vSelectedRow = $(paramEle).parents("tr");
    var vDataRow = gTableCustomer.row(vSelectedRow).data();
    console.log(vDataRow);
    gCustomerId = vDataRow.id;
    $('#modal-delete-customer').modal("show");
  }
  // Hàm xử lý khi click confirm delete
  function onBtnDeleteConfirmClick() {
    callApiDeleteCustomer();
    $('#modal-delete-customer').modal("hide");
  }
  // Hàm xử lý khi click detail order
  function onBtnDetailOrderClick(paramEle) {
    var vSelectedRow = $(paramEle).parents("tr");
    var vDataRow = gTableCustomer.row(vSelectedRow).data();
    console.log(vDataRow);
    gCustomerId = vDataRow.id;
    window.location.href = "customerOrder.html?id=" + vDataRow.id;
  }
  // Hàm xử lý khi click detail order
  function onBtnDetailPaymentClick(paramEle) {
    var vSelectedRow = $(paramEle).parents("tr");
    var vDataRow = gTableCustomer.row(vSelectedRow).data();
    console.log(vDataRow);
    gCustomerId = vDataRow.id;
    window.location.href = "customerPayment.html?id=" + vDataRow.id;
  }

  // Vùng 4: Khai báo hàm dùng chung
  // Hàm call api get all
  function callApiGetAllCustomer() {
    $.ajax({
      url: gUrl,
      type: "GET",
      dataType: "json",
      async: false,
      success: function(res) {
        gArrayCustomer = res;
        // console.log(gArrayCustomer);
      },
      error: function(error) {
        console.log(error.responseText);
      }
    });
  }
  // Hàm load data to table
  function loadDataToTable() {
    gTableCustomer.clear();
    gTableCustomer.rows.add(gArrayCustomer);
    gTableCustomer.draw();
  }
  // Hàm get data from input create
  function getDataFromInputCreate(paramObj) {
    paramObj.lastName = $('#inp-lastName').val();
    paramObj.firstName = $('#inp-firstName').val();
    paramObj.phoneNumber = $('#inp-phoneNumber').val();
    paramObj.address = $('#inp-address').val();
    paramObj.city = $('#inp-city').val();
    paramObj.state = $('#inp-state').val();
    paramObj.postalCode = $('#inp-postalCode').val();
    paramObj.country = $('#inp-country').val();
    paramObj.salesRepEmployeeNumber = $('#inp-salesRepEmployeeNumber').val();
    paramObj.creditLimit = $('#inp-creditLimit').val();
  }
  // Hàm validate dữ liệu
  function validateData(paramObj) {
    var vResult = true;
    if (paramObj.firstName == "") {
      alert("Phải nhập Firstname!");
      vResult = false;  
    }
    if (paramObj.lastName == "") {
      alert("Phải nhập Lastname!");
      vResult = false; 
    }
    return vResult;
  }
  // Hàm call api post
  function callApiPostNewCustomer(paramObj) {
    $.ajax({
      url: gUrl,
      data: JSON.stringify(paramObj),
      type: "POST",
      dataType: "json",
      contentType: 'application/json',
      async: false,
      success: function(res) {
        callApiGetAllCustomer();
        loadDataToTable();
        clearInpCreate();
      },
      error: function(error) {
        console.log(error.responseText);
      }
    });
  }
  // Hàm clear inp create
  function clearInpCreate() {
    $('#inp-lastName').val("");
    $('#inp-firstName').val("");
    $('#inp-phoneNumber').val("");
    $('#inp-address').val("");
    $('#inp-city').val("");
    $('#inp-state').val("");
    $('#inp-postalCode').val("");
    $('#inp-country').val("");
    $('#inp-salesRepEmployeeNumber').val("");
    $('#inp-creditLimit').val("");
  }
  // Hàm load data to modal update
  function loadDataToModalUpdate(paramData) {
    $('#inp-modal-customerId').val(paramData.id);
    $('#inp-modal-lastName').val(paramData.lastName);
    $('#inp-modal-firstName').val(paramData.firstName);
    $('#inp-modal-phoneNumber').val(paramData.phoneNumber);
    $('#inp-modal-address').val(paramData.address);
    $('#inp-modal-city').val(paramData.city);
    $('#inp-modal-state').val(paramData.state);
    $('#inp-modal-postalCode').val(paramData.postalCode);
    $('#inp-modal-country').val(paramData.country);
    $('#inp-modal-salesRepEmployeeNumber').val(paramData.salesRepEmployeeNumber);
    $('#inp-modal-creditLimit').val(paramData.creditLimit);
  }
  // Hàm get data from input update
  function getDataFromInputUpdate(paramObj) {
    paramObj.lastName = $('#inp-modal-lastName').val();
    paramObj.firstName = $('#inp-modal-firstName').val();
    paramObj.phoneNumber = $('#inp-modal-phoneNumber').val();
    paramObj.address = $('#inp-modal-address').val();
    paramObj.city = $('#inp-modal-city').val();
    paramObj.state = $('#inp-modal-state').val();
    paramObj.postalCode = $('#inp-modal-postalCode').val();
    paramObj.country = $('#inp-modal-country').val();
    paramObj.salesRepEmployeeNumber = $('#inp-modal-salesRepEmployeeNumber').val();
    paramObj.creditLimit = $('#inp-modal-creditLimit').val();
  }
  // Hàm call api put
  function callApiPutCustomerById(paramObj) {
    $.ajax({
      url: gUrl + "/" + gCustomerId,
      data: JSON.stringify(paramObj),
      type: "PUT",
      contentType: 'application/json',
      async: false,
      success: function(res) {
        callApiGetAllCustomer();
        loadDataToTable();
      },
      error: function(error) {
        console.log(error.responseText);
      }
    });
  }
  // Hàm call api delete
  function callApiDeleteCustomer() {
    $.ajax({
      url: gUrl + "/" + gCustomerId,
      type: "DELETE",
      async: false,
      success: function(res) {
        callApiGetAllCustomer();
        loadDataToTable();
      },
      error: function(error) {
        console.log(error.responseText);
      }
    });
  }
});