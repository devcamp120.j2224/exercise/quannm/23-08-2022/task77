$(document).ready(function() {
    // Vùng 1: Khai báo biến global
    var gUrl = "http://localhost:8080/orders";
    var gOrderId = 0;
    const gID_COL = 0;
    const gCOMMENTS_COL = 1;
    const gORDER_DATE_COL = 2;
    const gREQUIRED_DATE_COL = 3;
    const gSHIPPED_DATE_COL = 4;
    const gSTATUS_COL = 5;
    const gCUSTOMER_NAME_COL = 6;
    const gACTION_COL = 7;
    var gArrayOrder;
    var gArrayCustomer;
    var gNameCol = ["id", "comments", "orderDate", "requiredDate", "shippedDate", "status", "customer", "action"];
    var gTableOrder = $('#order-table').DataTable({
      columns: [
        { data: gNameCol[gID_COL] },
        { data: gNameCol[gCOMMENTS_COL] },
        { data: gNameCol[gORDER_DATE_COL] },
        { data: gNameCol[gREQUIRED_DATE_COL] },
        { data: gNameCol[gSHIPPED_DATE_COL] },
        { data: gNameCol[gSTATUS_COL] },
        { data: gNameCol[gCUSTOMER_NAME_COL] },
        { data: gNameCol[gACTION_COL] }
      ],
      columnDefs: [
        {
          targets: gCUSTOMER_NAME_COL,
          render: function(data) {
            return data.firstName + " " + data.lastName;
          }
        },
        {
          targets: gACTION_COL,
          defaultContent: `<i class="fas fa-edit text-primary" id="btn-update"></i>
          | <i class="fas fa-trash text-danger" id="btn-delete"></i>`
        }
      ],
      searching: false
    });
    var gHref = new URL(window.location);
    var gCustomerId = gHref.searchParams.get("id");
  
    // Vùng 2: Vùng gán
    onPageLoading();
    $('#create-order').on('click', onBtnCreateOrderClick);
    $('#order-table').on('click', "#btn-update", function() {
      onBtnUpdateOrderClick(this);
    });
    $('#update-order').on('click', onBtnUpdateModalClick);
    $('#order-table').on('click', "#btn-delete", function() {
      onBtnDeleteOrderClick(this);
    });
    $('#delete-order').on('click', onBtnDeleteConfirmClick);
  
    // Vùng 3: Khai báo hàm xử lý sự kiện
    // Hàm xử lý khi tải trang
    function onPageLoading() {
      callApiGetAllOrder();
      loadDataToTable();
      callApiGetAllCustomer();
      loadDataToSelectCustomer();
    }
    // Hàm xử lý khi click btn create new order
    function onBtnCreateOrderClick() {
      var vOrderObj = {
        comments: "",
        orderDate: "",
        requiredDate: "",
        shippedDate: "",
        status: "",
        customer: {
          id: 0
        }
      };
      getDataFromInputCreate(vOrderObj);
      // console.log(vOrderObj);
      var vIsCheck = validateData(vOrderObj);
      if (vIsCheck == true) {
        // console.log(vOrderObj);
        callApiPostNewOrder(vOrderObj);
      }
    }
    // Hàm xử lý khi click btn update order
    function onBtnUpdateOrderClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTableOrder.row(vSelectedRow).data();
      console.log(vDataRow);
      gOrderId = vDataRow.id;
      $('#modal-update-order').modal("show");
      loadDataToModalUpdate(vDataRow);
    }
    // Hàm xử lý khi click btn update in modal
    function onBtnUpdateModalClick() {
      var vOrderObj = {
        comments: "",
        orderDate: "",
        requiredDate: "",
        shippedDate: "",
        status: "",
        customer: {
          id: 0
        }
      };
      getDataFromInputUpdate(vOrderObj);
      // console.log(gCustomerId);
      // console.log(vCustomerObj);
      var vIsCheck = validateData(vOrderObj);
      if (vIsCheck == true) {
        callApiPutOrderById(vOrderObj);
        $('#modal-update-order').modal("hide");
      }
    }
    // Hàm xử lý khi click btn delete order
    function onBtnDeleteOrderClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTableOrder.row(vSelectedRow).data();
      console.log(vDataRow);
      gOrderId = vDataRow.id;
      $('#modal-delete-order').modal("show");
    }
    // Hàm xử lý khi click confirm delete
    function onBtnDeleteConfirmClick() {
      callApiDeleteOrder();
      $('#modal-delete-order').modal("hide");
    }
  
    // Vùng 4: Khai báo hàm dùng chung
    // Hàm call api get all
    function callApiGetAllOrder() {
      $.ajax({
        url: "http://localhost:8080/orders/customerId/" + gCustomerId,
        type: "GET",
        dataType: "json",
        async: false,
        success: function(res) {
          gArrayOrder = res;
          // console.log(gArrayCustomer);
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm load data to table
    function loadDataToTable() {
      gTableOrder.clear();
      gTableOrder.rows.add(gArrayOrder);
      gTableOrder.draw();
    }
    // Hàm get data from input create
    function getDataFromInputCreate(paramObj) {
      var today = new Date().toISOString();
      paramObj.comments = $('#inp-comments').val();
      paramObj.orderDate = today;
      paramObj.requiredDate = today;
      paramObj.shippedDate = today;
      $('#inp-orderDate').val(today);
      $('#inp-requiredDate').val(today);
      $('#inp-shippedDate').val(today);
      paramObj.status = $('#inp-status').val();
      paramObj.customer.id = $('#select-customer').find(":selected").val()
    }
    // Hàm validate dữ liệu
    function validateData(paramObj) {
      if (paramObj.customer.id == 0) {
        alert("Phải chọn Customer!");
        return false; 
      }
      return true;
    }
    // Hàm call api post
    function callApiPostNewOrder(paramObj) {
      $.ajax({
        url: gUrl,
        data: JSON.stringify(paramObj),
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          callApiGetAllOrder();
          loadDataToTable();
          clearInpCreate();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm clear inp create
    function clearInpCreate() {
      $('#inp-comments').val("");
      $('#inp-orderDate').val("");
      $('#inp-requiredDate').val("");
      $('#inp-shippedDate').val("");
      $('#inp-status').val("");
      $('#select-customer').val("0").prop('selected', true);
    }
    // Hàm load data to modal update
    function loadDataToModalUpdate(paramData) {
      $('#inp-modal-orderId').val(gOrderId)
      $('#inp-modal-comments').val(paramData.comments);
      $('#inp-modal-orderDate').val(paramData.orderDate);
      $('#inp-modal-requiredDate').val(paramData.requiredDate);
      $('#inp-modal-shippedDate').val(paramData.shippedDate);
      $('#inp-modal-status').val(paramData.status);
      $('#select-modal-customer').val(paramData.customer.id).prop('selected', true);
    }
    // Hàm get data from input update
    function getDataFromInputUpdate(paramObj) {
      paramObj.comments = $('#inp-modal-comments').val();
      paramObj.orderDate = $('#inp-modal-orderDate').val();
      paramObj.requiredDate = $('#inp-modal-requiredDate').val();
      paramObj.shippedDate = $('#inp-modal-shippedDate').val();
      paramObj.status = $('#inp-modal-status').val();
      paramObj.customer.id = $('#select-modal-customer').find(":selected").val()
    }
    // Hàm call api put
    function callApiPutOrderById(paramObj) {
      $.ajax({
        url: gUrl + "/" + gOrderId,
        data: JSON.stringify(paramObj),
        type: "PUT",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          callApiGetAllOrder();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm call api delete
    function callApiDeleteOrder() {
      $.ajax({
        url: gUrl + "/" + gOrderId,
        type: "DELETE",
        async: false,
        success: function(res) {
          callApiGetAllOrder();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm call api get all
    function callApiGetAllCustomer() {
      $.ajax({
        url: "http://localhost:8080/customers",
        type: "GET",
        dataType: "json",
        async: false,
        success: function(res) {
          gArrayCustomer = res;
          // console.log(gArrayCustomer);
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm load select customer
    function loadDataToSelectCustomer() {
      for (var i = 0; i < gArrayCustomer.length; i++) {
        $('#select-customer').append('<option value="' + gArrayCustomer[i].id + '">' + gArrayCustomer[i].firstName + " " + gArrayCustomer[i].lastName + '</option>');
        $('#select-modal-customer').append('<option value="' + gArrayCustomer[i].id + '">' + gArrayCustomer[i].firstName + " " + gArrayCustomer[i].lastName + '</option>');
      }
    }
  });