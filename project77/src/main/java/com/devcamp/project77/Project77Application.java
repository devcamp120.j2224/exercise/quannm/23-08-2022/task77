package com.devcamp.project77;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Project77Application {

	public static void main(String[] args) {
		SpringApplication.run(Project77Application.class, args);
	}

}
