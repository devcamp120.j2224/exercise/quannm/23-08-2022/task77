package com.devcamp.project77.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.project77.entity.Order;

@Repository
public interface IOrderRepository extends JpaRepository<Order, Integer> {
    List<Order> findByCustomerId(int id);

    @Query(value = "SELECT *, CONCAT_WS(' ', customers.first_name, customers.last_name) AS full_name FROM orders INNER JOIN customers ON orders.customer_id = customers.id HAVING full_name LIKE %:fullName%", nativeQuery = true)
    List<Order> findByFullNameLike(@Param("fullName") String fullName);

    @Query(value = "SELECT * FROM orders WHERE customer_id LIKE %:customerId%", nativeQuery = true)
    List<Order> findByCustomerIdLike(@Param("customerId") int customerId);
}
