package com.devcamp.project77.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.project77.entity.ProductLine;

@Repository
public interface IProductLineRepository extends JpaRepository<ProductLine, Integer> {
    
}
