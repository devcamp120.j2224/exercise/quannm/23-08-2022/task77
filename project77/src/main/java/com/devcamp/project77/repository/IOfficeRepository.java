package com.devcamp.project77.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.project77.entity.Office;

@Repository
public interface IOfficeRepository extends JpaRepository<Office, Integer> {
    
}
