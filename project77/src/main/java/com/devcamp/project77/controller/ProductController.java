package com.devcamp.project77.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.project77.entity.Product;
import com.devcamp.project77.service.ProductService;

@RestController
@CrossOrigin
public class ProductController {
    @Autowired
    ProductService pProductService;

    @GetMapping("/products")
    public List<Product> getAllProduct() {
        return pProductService.getAllProduct();
    }

    @GetMapping("/products/{id}")
    public Product getProductById(@PathVariable int id) {
        return pProductService.getProductById(id);
    }

    @PostMapping("/products")
    public ResponseEntity<Object> createNewProduct(@RequestBody Product pProduct) {
        try {
            return new ResponseEntity<>(pProductService.createNewProduct(pProduct), HttpStatus.CREATED);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<Object> updateProductById(@PathVariable("id") int id, @RequestBody Product pProduct) {
        if (pProductService.updateProductById(id, pProduct) != null) {
            try {
                return new ResponseEntity<>(HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<Product> deleteProductById(@PathVariable int id) {
        try {
            pProductService.deleteProductById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
