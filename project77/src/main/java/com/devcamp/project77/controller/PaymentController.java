package com.devcamp.project77.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.project77.entity.Payment;
import com.devcamp.project77.service.PaymentService;

@RestController
@CrossOrigin
public class PaymentController {
    @Autowired
    PaymentService pPaymentService;

    @GetMapping("/payments")
    public List<Payment> getAllPayment() {
        return pPaymentService.getAllPayment();
    }

    @GetMapping("/payments/{id}")
    public Payment getPaymentById(@PathVariable int id) {
        return pPaymentService.getPaymentById(id);
    }

    @PostMapping("/payments")
    public ResponseEntity<Object> createNewPayment(@RequestBody Payment pPayment) {
        try {
            return new ResponseEntity<>(pPaymentService.createNewPayment(pPayment), HttpStatus.CREATED);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/payments/{id}")
    public ResponseEntity<Object> updatePaymentById(@PathVariable("id") int id, @RequestBody Payment pPayment) {
        if (pPaymentService.updatePaymentById(id, pPayment) != null) {
            try {
                return new ResponseEntity<>(HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/payments/{id}")
    public ResponseEntity<Payment> deleteCustomerById(@PathVariable int id) {
        try {
            pPaymentService.deletePaymentById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/payments/customerId/{id}")
    public ResponseEntity<List<Payment>> getOrderByCustomerId(@PathVariable int id) {
        try {
            return new ResponseEntity<List<Payment>>(pPaymentService.getOrderByCustomerId(id), HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

