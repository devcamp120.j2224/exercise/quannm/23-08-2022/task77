package com.devcamp.project77.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.project77.entity.OrderDetail;
import com.devcamp.project77.service.OrderDetailService;

@RestController
@CrossOrigin
public class OrderDetailController {
    @Autowired
    OrderDetailService pOrderDetailService;

    @GetMapping("/orderDetails")
    public List<OrderDetail> getAllOrderDetail() {
        return pOrderDetailService.getAllOrderDetail();
    }

    @GetMapping("/orderDetails/{id}")
    public OrderDetail getOrderDetailById(@PathVariable int id) {
        return pOrderDetailService.getOrderDetailById(id);
    }

    @PostMapping("/orderDetails")
    public ResponseEntity<Object> createNewOrderDetail(@RequestBody OrderDetail pOrderDetail) {
        try {
            return new ResponseEntity<>(pOrderDetailService.createNewOrderDetail(pOrderDetail), HttpStatus.CREATED);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/orderDetails/{id}")
    public ResponseEntity<Object> updateOrderDetailById(@PathVariable("id") int id, @RequestBody OrderDetail pOrderDetail) {
        if (pOrderDetailService.updateOrderDetailById(id, pOrderDetail) != null) {
            try {
                return new ResponseEntity<>(HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/orderDetails/{id}")
    public ResponseEntity<OrderDetail> deleteOrderById(@PathVariable int id) {
        try {
            pOrderDetailService.deleteOrderDetailById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/orderDetails/orderId/{id}")
    public ResponseEntity<List<OrderDetail>> getOrderDetailByOrderId(@PathVariable int id) {
        try {
            return new ResponseEntity<List<OrderDetail>>(pOrderDetailService.getOrderDetailByOrderId(id), HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
