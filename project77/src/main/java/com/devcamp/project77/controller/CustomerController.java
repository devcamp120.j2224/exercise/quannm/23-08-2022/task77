package com.devcamp.project77.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.project77.entity.Customer;
import com.devcamp.project77.service.CustomerService;

@RestController
@CrossOrigin
public class CustomerController {
    @Autowired
    CustomerService pCustomerService;

    @GetMapping("/customers")
    public List<Customer> getAllCustomer() {
        return pCustomerService.getAllCustomer();
    }

    @GetMapping("/customers/{id}")
    public Customer getCustomerById(@PathVariable int id) {
        return pCustomerService.getCustomerById(id);
    }

    @PostMapping("/customers")
    public ResponseEntity<Object> createNewCustomer(@RequestBody Customer pCustomer) {
        try {
            return new ResponseEntity<>(pCustomerService.createNewCustomer(pCustomer), HttpStatus.CREATED);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/customers/{id}")
    public ResponseEntity<Object> updateCustomerById(@PathVariable("id") int id, @RequestBody Customer pCustomer) {
        if (pCustomerService.updateCustomerById(id, pCustomer) != null) {
            try {
                return new ResponseEntity<>(HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/customers/{id}")
    public ResponseEntity<Customer> deleteCustomerById(@PathVariable int id) {
        try {
            pCustomerService.deleteCustomerById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
