package com.devcamp.project77.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.project77.entity.ProductLine;
import com.devcamp.project77.service.ProductLineService;

@RestController
@CrossOrigin
public class ProductLineController {
    @Autowired
    ProductLineService pProductLineService;

    @GetMapping("/productLines")
    public List<ProductLine> getAllProductLine() {
        return pProductLineService.getAllProductLine();
    }

    @GetMapping("/productLines/{id}")
    public ProductLine getProductLineById(@PathVariable int id) {
        return pProductLineService.getProductLineById(id);
    }

    @PostMapping("/productLines")
    public ResponseEntity<Object> createNewProductLine(@RequestBody ProductLine pProductLine) {
        try {
            return new ResponseEntity<>(pProductLineService.createNewProductLine(pProductLine), HttpStatus.CREATED);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/productLines/{id}")
    public ResponseEntity<Object> updateProductLineById(@PathVariable("id") int id, @RequestBody ProductLine pProductLine) {
        if (pProductLineService.updateProductLineById(id, pProductLine) != null) {
            try {
                return new ResponseEntity<>(HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/productLines/{id}")
    public ResponseEntity<ProductLine> deleteProductLineById(@PathVariable int id) {
        try {
            pProductLineService.deleteProductLineById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
