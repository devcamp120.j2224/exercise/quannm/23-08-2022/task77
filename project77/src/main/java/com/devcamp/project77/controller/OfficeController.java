package com.devcamp.project77.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.project77.entity.Office;
import com.devcamp.project77.service.OfficeService;

@RestController
@CrossOrigin
public class OfficeController {
    @Autowired
    OfficeService pOfficeService;

    @GetMapping("/offices")
    public List<Office> getAllOffice() {
        return pOfficeService.getAllOffice();
    }

    @GetMapping("/offices/{id}")
    public Office getOfficeById(@PathVariable int id) {
        return pOfficeService.getOfficeById(id);
    }

    @PostMapping("/offices")
    public ResponseEntity<Object> createNewOffice(@RequestBody Office pOffice) {
        try {
            return new ResponseEntity<>(pOfficeService.createNewOffice(pOffice), HttpStatus.CREATED);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/offices/{id}")
    public ResponseEntity<Object> updateOfficeById(@PathVariable("id") int id, @RequestBody Office pOffice) {
        if (pOfficeService.updateOfficeById(id, pOffice) != null) {
            try {
                return new ResponseEntity<>(HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/offices/{id}")
    public ResponseEntity<Office> deleteOfficeById(@PathVariable int id) {
        try {
            pOfficeService.deleteOfficeById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
