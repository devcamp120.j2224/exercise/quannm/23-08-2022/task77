package com.devcamp.project77.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.project77.entity.Employee;
import com.devcamp.project77.service.EmployeeService;

@RestController
@CrossOrigin
public class EmployeeController {
    @Autowired
    EmployeeService pEmployeeService;

    @GetMapping("/employees")
    public List<Employee> getAllEmployee() {
        return pEmployeeService.getAllEmployee();
    }

    @GetMapping("/employees/{id}")
    public Employee getEmployeeById(@PathVariable int id) {
        return pEmployeeService.getEmployeeById(id);
    }

    @PostMapping("/employees")
    public ResponseEntity<Object> createNewEmployee(@RequestBody Employee pEmployee) {
        try {
            return new ResponseEntity<>(pEmployeeService.createNewEmployee(pEmployee), HttpStatus.CREATED);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/employees/{id}")
    public ResponseEntity<Object> updateEmployeeById(@PathVariable("id") int id, @RequestBody Employee pEmployee) {
        if (pEmployeeService.updateEmployeeById(id, pEmployee) != null) {
            try {
                return new ResponseEntity<>(HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/employees/{id}")
    public ResponseEntity<Employee> deleteEmployeeById(@PathVariable int id) {
        try {
            pEmployeeService.deleteEmployeeById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
