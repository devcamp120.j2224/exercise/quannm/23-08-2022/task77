package com.devcamp.project77.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.project77.entity.Order;
import com.devcamp.project77.service.OrderService;

@RestController
@CrossOrigin
public class OrderController {
    @Autowired
    OrderService pOrderService;

    @GetMapping("/orders")
    public List<Order> getAllOrder() {
        return pOrderService.getAllOrder();
    }

    @GetMapping("/orders/{id}")
    public Order getOrderById(@PathVariable int id) {
        return pOrderService.getOrderById(id);
    }

    @PostMapping("/orders")
    public ResponseEntity<Object> createNewOrder(@RequestBody Order pOrder) {
        try {
            return new ResponseEntity<>(pOrderService.createNewOrder(pOrder), HttpStatus.CREATED);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/orders/{id}")
    public ResponseEntity<Object> updateOrderById(@PathVariable("id") int id, @RequestBody Order pOrder) {
        if (pOrderService.updateOrderById(id, pOrder) != null) {
            try {
                return new ResponseEntity<>(HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Order> deleteOrderById(@PathVariable int id) {
        try {
            pOrderService.deleteOrderById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/orders/customerId/{id}")
    public ResponseEntity<List<Order>> getOrderByCustomerId(@PathVariable int id) {
        try {
            return new ResponseEntity<List<Order>>(pOrderService.getOrderByCustomerId(id), HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/orders/fullName")
    public ResponseEntity<List<Order>> getOrderByCustomerFullName(@RequestParam String fullName) {
        try {
            return new ResponseEntity<List<Order>>(pOrderService.getOrderByCustomerFullName(fullName), HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/orders/customerId")
    public ResponseEntity<List<Order>> getOrderByCustomerIdLike(@RequestParam int customerId) {
        try {
            return new ResponseEntity<List<Order>>(pOrderService.getOrderByCustomerIdLike(customerId), HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
