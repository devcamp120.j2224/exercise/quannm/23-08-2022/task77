package com.devcamp.project77.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.project77.entity.Customer;
import com.devcamp.project77.repository.ICustomerRepository;

@Service
public class CustomerService {
    @Autowired
    ICustomerRepository pCustomerRepository;

    public List<Customer> getAllCustomer() {
        return pCustomerRepository.findAll();
    }

    public Customer getCustomerById(int id) {
        Optional<Customer> customerData = pCustomerRepository.findById(id);
        if (customerData.isPresent()) {
            return customerData.get();
        } else {
            return null;
        }
    }

    public Customer updateCustomerById(int id, Customer pCustomer) {
        Optional<Customer> customerData = pCustomerRepository.findById(id);
        if (customerData.isPresent()) {
            Customer customerUp = customerData.get();
            customerUp.setAddress(pCustomer.getAddress());
            customerUp.setCity(pCustomer.getCity());
            customerUp.setCountry(pCustomer.getCountry());
            customerUp.setCreditLimit(pCustomer.getCreditLimit());
            customerUp.setFirstName(pCustomer.getFirstName());
            customerUp.setLastName(pCustomer.getLastName());
            customerUp.setPhoneNumber(pCustomer.getPhoneNumber());
            customerUp.setPostalCode(pCustomer.getPostalCode());
            customerUp.setState(pCustomer.getState());
            customerUp.setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
            return pCustomerRepository.save(customerUp);
        } else {
            return null;
        }
    }

    public Customer createNewCustomer(Customer pCustomer) {
        Customer customerUp = new Customer();
        customerUp.setAddress(pCustomer.getAddress());
        customerUp.setCity(pCustomer.getCity());
        customerUp.setCountry(pCustomer.getCountry());
        customerUp.setCreditLimit(pCustomer.getCreditLimit());
        customerUp.setFirstName(pCustomer.getFirstName());
        customerUp.setLastName(pCustomer.getLastName());
        customerUp.setPhoneNumber(pCustomer.getPhoneNumber());
        customerUp.setPostalCode(pCustomer.getPostalCode());
        customerUp.setState(pCustomer.getState());
        customerUp.setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
        return pCustomerRepository.save(customerUp);
    }

    public void deleteCustomerById(int id) {
        pCustomerRepository.deleteById(id);
    }
}
