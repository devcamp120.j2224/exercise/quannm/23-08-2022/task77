package com.devcamp.project77.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.project77.entity.ProductLine;
import com.devcamp.project77.repository.IProductLineRepository;

@Service
public class ProductLineService {
    @Autowired
    IProductLineRepository pProductLineRepository;

    public List<ProductLine> getAllProductLine() {
        return pProductLineRepository.findAll();
    }

    public ProductLine getProductLineById(int id) {
        Optional<ProductLine> productLineData = pProductLineRepository.findById(id);
        if (productLineData.isPresent()) {
            return productLineData.get();
        } else {
            return null;
        }
    }

    public ProductLine updateProductLineById(int id, ProductLine pProductLine) {
        Optional<ProductLine> productLineData = pProductLineRepository.findById(id);
        if (productLineData.isPresent()) {
            ProductLine productLineUp = productLineData.get();
            productLineUp.setDescription(pProductLine.getDescription());
            productLineUp.setProductLine(pProductLine.getProductLine());
            productLineUp.setProducts(pProductLine.getProducts());
            return pProductLineRepository.save(productLineUp);
        } else {
            return null;
        }
    }

    public ProductLine createNewProductLine(ProductLine pProductLine) {
        ProductLine productLineUp = new ProductLine();
        productLineUp.setDescription(pProductLine.getDescription());
        productLineUp.setProductLine(pProductLine.getProductLine());
        productLineUp.setProducts(pProductLine.getProducts());
        return pProductLineRepository.save(productLineUp);
    }

    public void deleteProductLineById(int id) {
        pProductLineRepository.deleteById(id);
    }
}
