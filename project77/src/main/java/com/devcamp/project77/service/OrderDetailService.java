package com.devcamp.project77.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.project77.entity.OrderDetail;
import com.devcamp.project77.repository.IOrderDetailRepository;

@Service
public class OrderDetailService {
    @Autowired
    IOrderDetailRepository pOrderDetailRepository;

    public List<OrderDetail> getAllOrderDetail() {
        return pOrderDetailRepository.findAll();
    }

    public OrderDetail getOrderDetailById(int id) {
        Optional<OrderDetail> orderDetailData = pOrderDetailRepository.findById(id);
        if (orderDetailData.isPresent()) {
            return orderDetailData.get();
        } else {
            return null;
        }
    }

    public OrderDetail updateOrderDetailById(int id, OrderDetail pOrderDetail) {
        Optional<OrderDetail> orderDetailData = pOrderDetailRepository.findById(id);
        if (orderDetailData.isPresent()) {
            OrderDetail orderDetailUp = orderDetailData.get();
            orderDetailUp.setOrder(pOrderDetail.getOrder());
            orderDetailUp.setPriceEach(pOrderDetail.getPriceEach());
            orderDetailUp.setProduct(pOrderDetail.getProduct());
            orderDetailUp.setQuantityOrder(pOrderDetail.getQuantityOrder());
            return pOrderDetailRepository.save(orderDetailUp);
        } else {
            return null;
        }
    }

    public OrderDetail createNewOrderDetail(OrderDetail pOrderDetail) {
        OrderDetail orderDetailUp = new OrderDetail();
        orderDetailUp.setOrder(pOrderDetail.getOrder());
        orderDetailUp.setPriceEach(pOrderDetail.getPriceEach());
        orderDetailUp.setProduct(pOrderDetail.getProduct());
        orderDetailUp.setQuantityOrder(pOrderDetail.getQuantityOrder());
        return pOrderDetailRepository.save(orderDetailUp);
    }

    public void deleteOrderDetailById(int id) {
        pOrderDetailRepository.deleteById(id);
    }

    public List<OrderDetail> getOrderDetailByOrderId(int id) {
        List<OrderDetail> listOrderDetails = new ArrayList<>();
        pOrderDetailRepository.findByOrderId(id).forEach(listOrderDetails::add);
        return listOrderDetails;
    }
}
