package com.devcamp.project77.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.project77.entity.Product;
import com.devcamp.project77.repository.IProductRepository;

@Service
public class ProductService {
    @Autowired
    IProductRepository pProductRepository;

    public List<Product> getAllProduct() {
        return pProductRepository.findAll();
    }

    public Product getProductById(int id) {
        Optional<Product> productData = pProductRepository.findById(id);
        if (productData.isPresent()) {
            return productData.get();
        } else {
            return null;
        }
    }

    public Product updateProductById(int id, Product pProduct) {
        Optional<Product> productData = pProductRepository.findById(id);
        if (productData.isPresent()) {
            Product productUp = productData.get();
            productUp.setBuyPrice(pProduct.getBuyPrice());
            productUp.setIdProductLine(pProduct.getIdProductLine());
            productUp.setOrderDetails(pProduct.getOrderDetails());
            productUp.setProductCode(pProduct.getProductCode());
            productUp.setProductDescripttion(pProduct.getProductDescripttion());
            productUp.setProductLine(pProduct.getProductLine());
            productUp.setProductName(pProduct.getProductName());
            productUp.setProductScale(pProduct.getProductScale());
            productUp.setProductVendor(pProduct.getProductVendor());
            productUp.setQuantityInStock(pProduct.getQuantityInStock());
            return pProductRepository.save(productUp);
        } else {
            return null;
        }
    }

    public Product createNewProduct(Product pProduct) {
        Product productUp = new Product();
        productUp.setBuyPrice(pProduct.getBuyPrice());
        productUp.setIdProductLine(pProduct.getIdProductLine());
        productUp.setOrderDetails(pProduct.getOrderDetails());
        productUp.setProductCode(pProduct.getProductCode());
        productUp.setProductDescripttion(pProduct.getProductDescripttion());
        productUp.setProductLine(pProduct.getProductLine());
        productUp.setProductName(pProduct.getProductName());
        productUp.setProductScale(pProduct.getProductScale());
        productUp.setProductVendor(pProduct.getProductVendor());
        productUp.setQuantityInStock(pProduct.getQuantityInStock());
        return pProductRepository.save(productUp);
    }

    public void deleteProductById(int id) {
        pProductRepository.deleteById(id);
    }
}
