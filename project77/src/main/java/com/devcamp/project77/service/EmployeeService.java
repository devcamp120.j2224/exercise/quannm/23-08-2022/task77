package com.devcamp.project77.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.project77.entity.Employee;
import com.devcamp.project77.repository.IEmployeeRepository;

@Service
public class EmployeeService {
    @Autowired
    IEmployeeRepository pEmployeeRepository;

    public List<Employee> getAllEmployee() {
        return pEmployeeRepository.findAll();
    }

    public Employee getEmployeeById(int id) {
        Optional<Employee> employeeData = pEmployeeRepository.findById(id);
        if (employeeData.isPresent()) {
            return employeeData.get();
        } else {
            return null;
        }
    }

    public Employee updateEmployeeById(int id, Employee pEmployee) {
        Optional<Employee> employeeData = pEmployeeRepository.findById(id);
        if (employeeData.isPresent()) {
            Employee employeeUp = employeeData.get();
            employeeUp.setEmail(pEmployee.getEmail());
            employeeUp.setExtension(pEmployee.getExtension());
            employeeUp.setFirstName(pEmployee.getFirstName());
            employeeUp.setJobTitle(pEmployee.getJobTitle());
            employeeUp.setLastName(pEmployee.getLastName());
            employeeUp.setOfficeCode(pEmployee.getOfficeCode());
            employeeUp.setReportTo(pEmployee.getReportTo());
            return pEmployeeRepository.save(employeeUp);
        } else {
            return null;
        }
    }

    public Employee createNewEmployee(Employee pEmployee) {
        Employee employeeUp = new Employee();
        employeeUp.setEmail(pEmployee.getEmail());
        employeeUp.setExtension(pEmployee.getExtension());
        employeeUp.setFirstName(pEmployee.getFirstName());
        employeeUp.setJobTitle(pEmployee.getJobTitle());
        employeeUp.setLastName(pEmployee.getLastName());
        employeeUp.setOfficeCode(pEmployee.getOfficeCode());
        employeeUp.setReportTo(pEmployee.getReportTo());
        return pEmployeeRepository.save(employeeUp);
    }

    public void deleteEmployeeById(int id) {
        pEmployeeRepository.deleteById(id);
    }
}
