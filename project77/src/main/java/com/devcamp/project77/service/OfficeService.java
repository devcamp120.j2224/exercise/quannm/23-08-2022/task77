package com.devcamp.project77.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.project77.entity.Office;
import com.devcamp.project77.repository.IOfficeRepository;

@Service
public class OfficeService {
    @Autowired
    IOfficeRepository pOfficeRepository;

    public List<Office> getAllOffice() {
        return pOfficeRepository.findAll();
    }

    public Office getOfficeById(int id) {
        Optional<Office> officeData = pOfficeRepository.findById(id);
        if (officeData.isPresent()) {
            return officeData.get();
        } else {
            return null;
        }
    }

    public Office updateOfficeById(int id, Office pOffice) {
        Optional<Office> officeData = pOfficeRepository.findById(id);
        if (officeData.isPresent()) {
            Office officeUp = officeData.get();
            officeUp.setAddressLine(pOffice.getAddressLine());
            officeUp.setCity(pOffice.getCity());
            officeUp.setCountry(pOffice.getCountry());
            officeUp.setPhone(pOffice.getPhone());
            officeUp.setState(pOffice.getState());
            officeUp.setTerritory(pOffice.getTerritory());
            return pOfficeRepository.save(officeUp);
        } else {
            return null;
        }
    }

    public Office createNewOffice(Office pOffice) {
        Office officeUp = new Office();
        officeUp.setAddressLine(pOffice.getAddressLine());
        officeUp.setCity(pOffice.getCity());
        officeUp.setCountry(pOffice.getCountry());
        officeUp.setPhone(pOffice.getPhone());
        officeUp.setState(pOffice.getState());
        officeUp.setTerritory(pOffice.getTerritory());
        return pOfficeRepository.save(officeUp);
    }

    public void deleteOfficeById(int id) {
        pOfficeRepository.deleteById(id);
    }
}
