package com.devcamp.project77.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.project77.entity.Order;
import com.devcamp.project77.repository.IOrderRepository;

@Service
public class OrderService {
    @Autowired
    IOrderRepository pOrderRepository;

    public List<Order> getAllOrder() {
        return pOrderRepository.findAll();
    }

    public Order getOrderById(int id) {
        Optional<Order> orderData = pOrderRepository.findById(id);
        if (orderData.isPresent()) {
            return orderData.get();
        } else {
            return null;
        }
    }

    public Order updateOrderById(int id, Order pOrder) {
        Optional<Order> orderData = pOrderRepository.findById(id);
        if (orderData.isPresent()) {
            Order orderUp = orderData.get();
            orderUp.setComments(pOrder.getComments());
            orderUp.setCustomer(pOrder.getCustomer());
            orderUp.setOrderDate(pOrder.getOrderDate());
            orderUp.setOrderDetails(pOrder.getOrderDetails());
            orderUp.setRequiredDate(pOrder.getRequiredDate());
            orderUp.setShippedDate(pOrder.getShippedDate());
            orderUp.setStatus(pOrder.getStatus());
            return pOrderRepository.save(orderUp);
        } else {
            return null;
        }
    }

    public Order createNewOrder(Order pOrder) {
        Order orderUp = new Order();
        orderUp.setComments(pOrder.getComments());
        orderUp.setCustomer(pOrder.getCustomer());
        orderUp.setOrderDate(pOrder.getOrderDate());
        orderUp.setOrderDetails(pOrder.getOrderDetails());
        orderUp.setRequiredDate(pOrder.getRequiredDate());
        orderUp.setShippedDate(pOrder.getShippedDate());
        orderUp.setStatus(pOrder.getStatus());
        return pOrderRepository.save(orderUp);
    }

    public void deleteOrderById(int id) {
        pOrderRepository.deleteById(id);
    }

    public List<Order> getOrderByCustomerId(int id) {
        List<Order> listOrders = new ArrayList<>();
        pOrderRepository.findByCustomerId(id).forEach(listOrders::add);
        return listOrders;
    }

    public List<Order> getOrderByCustomerFullName(String fullName) {
        List<Order> listOrders = new ArrayList<>();
        pOrderRepository.findByFullNameLike(fullName).forEach(listOrders::add);
        return listOrders;
    }

    public List<Order> getOrderByCustomerIdLike(int customerId) {
        List<Order> listOrders = new ArrayList<>();
        pOrderRepository.findByCustomerIdLike(customerId).forEach(listOrders::add);
        return listOrders;
    }
}
