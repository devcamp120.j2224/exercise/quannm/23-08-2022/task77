package com.devcamp.project77.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.project77.entity.Payment;
import com.devcamp.project77.repository.IPaymentRepository;

@Service
public class PaymentService {
    @Autowired
    IPaymentRepository pPaymentRepository;

    public List<Payment> getAllPayment() {
        return pPaymentRepository.findAll();
    }

    public Payment getPaymentById(int id) {
        Optional<Payment> paymentData = pPaymentRepository.findById(id);
        if (paymentData.isPresent()) {
            return paymentData.get();
        } else {
            return null;
        }
    }

    public Payment updatePaymentById(int id, Payment pPayment) {
        Optional<Payment> paymentData = pPaymentRepository.findById(id);
        if (paymentData.isPresent()) {
            Payment paymentUp = paymentData.get();
            paymentUp.setAmmount(pPayment.getAmmount());
            paymentUp.setCheckNumber(pPayment.getCheckNumber());
            paymentUp.setCustomer(pPayment.getCustomer());
            paymentUp.setPaymentDate(pPayment.getPaymentDate());
            return pPaymentRepository.save(paymentUp);
        } else {
            return null;
        }
    }

    public Payment createNewPayment(Payment pPayment) {
        Payment paymentUp = new Payment();
        paymentUp.setAmmount(pPayment.getAmmount());
        paymentUp.setCheckNumber(pPayment.getCheckNumber());
        paymentUp.setCustomer(pPayment.getCustomer());
        paymentUp.setPaymentDate(pPayment.getPaymentDate());
        return pPaymentRepository.save(paymentUp);
    }

    public void deletePaymentById(int id) {
        pPaymentRepository.deleteById(id);
    }

    public List<Payment> getOrderByCustomerId(int id) {
        List<Payment> listPayments = new ArrayList<>();
        pPaymentRepository.findByCustomerId(id).forEach(listPayments::add);
        return listPayments;
    }
}
